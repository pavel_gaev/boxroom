var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var http = require('http');
var url = require('url');

var app = express();

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

app.use(express.static('.'));

var HOST = 'pgaev';
var PORT = '8080';
var currentVersion = "1.0";
var RESTApiUrls = {
    getListByUserId: '/' + currentVersion + '/rentItems',
    insertItem: '/' + currentVersion + '/rentItems/add',
    deleteItem: '/' + currentVersion + '/rentItems/delete/:itemId',
    putItem: '/' + currentVersion + '/rentItems/put/:itemId'
};


// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router


router.get('/', function (request, response) {
    response.end();
});

router.post('/rentPeriod/:itemId', function (request, response) {
    console.log('match request');
    var clientRequest = request;
    var clientResponse = response;
    var data = JSON.stringify(clientRequest.body);
    var itemId = request.params.itemId;
    var options = {
        host: HOST,
        port: PORT,
        method: 'MATCH',
        path: '/rentItem/' + itemId
    };
    var request = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            clientResponse.end();
        });
    });
    request.on('error', function (e) {
        console.log(e.message);
        clientResponse.statusCode = 500;
        clientResponse.end();
    });
    request.write(data);
    request.end();
});

router.post(RESTApiUrls.putItem, function (request, response) {
    console.log('put request');
    var clientRequest = request;
    var clientResponse = response;
    var data = JSON.stringify(clientRequest.body);
    var itemId = request.params.itemId;
    console.log(itemId);
    var options = {
        host: HOST,
        port: PORT,
        method: 'PUT',
        path: '/rentItem/' + itemId
    };
    var request = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            clientResponse.end();
        });
    });
    request.on('error', function (e) {
        console.log(e.message);
        clientResponse.statusCode = 500;
        clientResponse.end();
    });
    request.write(data);
    request.end();
});

router.post(RESTApiUrls.deleteItem, function (request, response) {
    console.log('delete request');
    var clientRequest = request;
    var clientResponse = response;
    var data = JSON.stringify(clientRequest.body);
    var itemId = request.params.itemId;

    var options = {
        host: HOST,
        port: PORT,
        method: 'DELETE',
        path: '/rentItem/' + itemId
    };
    var request = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            data += chunk;
        });
        res.on('end', function () {
            clientResponse.end();
        });
    });
    request.on('error', function (e) {
        console.log(e.message);
        clientResponse.statusCode = 500;
        clientResponse.end();
    });

    request.end();
});

router.post(RESTApiUrls.insertItem, function (request, response) {
    var clientRequest = request;
    var clientResponse = response;
    var data = JSON.stringify(clientRequest.body);
    var options = {
        host: HOST,
        port: PORT,
        method: 'POST',
        path: '/rentItem',
        headers: {
            'Content-Type': 'application/json',
            'Content-Length': Buffer.byteLength(data)
        }
    };
    var request = http.request(options, function (res) {
        res.setEncoding('utf8');
        var data = '';
        res.on('data', function (chunk) {
            console.log(chunk);
            data += chunk;
        });
        res.on('end', function () {
            console.log(res.body);
            clientResponse.end(data);
        });
    });
    request.on('error', function (e) {
        console.log(e.message);
        clientResponse.statusCode = 500;
        clientResponse.end();
    });
    request.write(data);
    request.end();
});

router.get(RESTApiUrls.getListByUserId, function (request, response) {
    var clientResponse = response;

    var options = {
        host: HOST,
        port: PORT,
        path: '/rentItem/search/findByOwnerId?id=' + request.query.userId
    };
    var request = http.request(options, function (res) {
        var responseReady = false;
        var data = '';

        res.on('data', function (chunk) {
            responseReady = true;
            data += chunk;
        });
        res.on('end', function () {
            //clientResponse.json(data._embedded.items);
            clientResponse.json((JSON.parse(data))._embedded.items);
        });
    });
    request.on('error', function (e) {
        console.log(e.message);
        //clientResponse.json([{
        //  _links: {
        //    rentItem: {
        //      href: 'http://test/-1'
        //    }
        //  },
        //  name: 'testTaskFromMoc',
        //  description: 'testDesc'
        //}]);
    });
    request.end();

});

app.use('/api/', router);


module.exports = app;
