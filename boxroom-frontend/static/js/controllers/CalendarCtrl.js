/**
 * Created by AKoryagin on 20.03.2016.
 */
angular.module('boxroom')
    .controller('CalendarCtrl', ['$scope', '$http', 'rentObjectsStore', function($scope, $http, rentObjectsStore) {
        $scope.nowDate = moment();
        $scope.periods = rentObjectsStore;
        $scope.calendarTitle = false;
        $scope.bookedSince = moment().format('YYYY.MM.DD');
        $scope.bookedFor = moment((new Date()).setDate(29)).format('YYYY.MM.DD');

        $scope.submitDate = function() {
            $http.post('http://pgaev:8080/rentPeriod', {
                itemId: -1,
                startDate: moment().format('YYYY-MM-DDT00:00:00.000ZZ'),
                endDate: moment((new Date()).setDate(0)).format('YYYY-MM-DDT00:00:00.000ZZ'),
                direction: 'OUT',
                customerId: -1,
                status: 'NEW'
            }).then(function() {
                console.log(rentObjectsStore);
            });
        };

        $scope.eventClicked = function() {
            console.log('here');
            console.log(arguments);
        };
        $scope.eventApprove = function(event) {
            event.type = 'success';
            event.editable = false;
            event.deletable = false;
            $http.patch('api/rentPeriod/' + event.id, {status: 'APPROVED'});
        };
        $scope.eventDecline = function(event) {
            console.log(event);
            event.type = 'warning';
            event.editable = false;
            event.deletable = false;
            $http.patch('api/rentPeriod/' + event.id, {status: 'DECLINED'});
        };

        $scope.cellModifier = function(cell) {

            var cssClass = 'calendar-cell-';
            var eventTitle;
            if (!cell.events[0]) return;

            switch (cell.events[0].type) {
                case 'warning':
                    cssClass += 'warning';
                    eventTitle = 'Отклонено';
                    break;
                case 'info':
                    cssClass += 'info';
                    eventTitle = 'Требует подтверждения';
                    break;
                case "success":
                    cssClass += 'success';
                    eventTitle = 'Забронировано';
                    break;
                case "important":
                    cssClass += 'important';
                    eventTitle = 'Возвращено';
                    break;
                case "special":
                    cssClass += 'special';
                    eventTitle = 'Выдано';
                    break;
                default:
                    cssClass += 'default';
            }
            cell.cssClass = cssClass;
            cell.events[0].title = eventTitle;
            cell.label = '-' + cell.label + '-';
        };



    }]);