/**
 * Created by AKoryagin on 19.03.2016.
 */
angular.module('boxroom')
    .controller('MainLoggedUserPageCtrl', ['$scope', '$http', '$stateParams', '$filter', 'rentObjectsStore', 'MOK_DATA', function($scope, $http, $stateParams, $filter, rentObjectsStore, MOK_DATA) {
        $scope.thingsForRent = rentObjectsStore.things;

        $scope.timestamps = [];
        $scope.isBarcodeScannerInput = false;

        $scope.thingsSearchString = '';

        $scope.allowRedact = true;

        $scope.itemDescriptionIsShown = false;

        $scope.descriptionItem = null;

        $scope.searchThings = function() {
            if ($scope.timestamps[6] - $scope.timestamps[0] < 300) {
                $scope.isBarcodeScannerInput = true;
            }
            $scope.timestamps = [];
            rentObjectsStore.search($scope.thingsSearchString).then(function() {
                if (rentObjectsStore.things.length == 1 && $scope.isBarcodeScannerInput) {
                    window.location.replace("#/viewCalendar/" + rentObjectsStore.things[0].ownerId);
                }
            });

            $scope.descriptionItem = null;
        };


        $scope.inputByKeyboard = function(KeyboardEvent) {
            if ($scope.timestamps.length < 7) {
                $scope.timestamps.push(KeyboardEvent.timeStamp)
            }

        };

        $scope.toggleDescription = function(thing) {
            if ($scope.descriptionItem && $scope.descriptionItem.id === thing.id) {
                $scope.descriptionItem = null;
                return;
            }
            $scope.descriptionItem = thing;
        };

        $scope.addItem = function() {
            $scope.descriptionItem = null;
            var newItem = {
                name: '',
                description: '',
                ownerId: MOK_DATA.USER_ID,
                isActive: true,
                isNew: true
            };
            $scope.thingsForRent.push(newItem);
            $scope.toggleDescription(newItem);
        };

        $scope.discarbAdding = function(thing) {
            $scope.thingsForRent.splice($scope.thingsForRent.indexOf(thing), 1);
            $scope.descriptionItem = null;
        };

        $scope.saveNewItem = function(thing) {
            rentObjectsStore.insert(thing);
        };

        $scope.$watch('descriptionItem', function(newVal, oldVal) {
            if (newVal) {
                $scope.itemDescriptionIsShown = true;
            } else {
                $scope.itemDescriptionIsShown = false;
            }
        });

        $scope.$on('deleteItem', function(event,itemObject) {
            rentObjectsStore.delete(itemObject).then(function() {
                $scope.descriptionItem = null;
            })
        });
        $scope.$on('saveNewItem', function(event,itemObject) {
            $scope.saveNewItem(itemObject);
        });
        $scope.$on('putItem', function(event,itemObject) {
            rentObjectsStore.put(itemObject);
        })
    }]);