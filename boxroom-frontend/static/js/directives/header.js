angular.module('boxroom')
    .directive('mainHeader', function () {
        return {
            templateUrl: 'static/templates/header.html'
        }
    });