angular.module('boxroom')
    .directive('itemDescription', function () {
        return {
            templateUrl: 'static/templates/itemDescription.html',
            controller: 'ItemDescriptionCtrl',
            scope: {
                showed: '=',
                item: '='
            },

            link: function(scope, element, attrs, ctrl) {

                scope.allowRedact = scope.$parent.allowRedact;
            }
        }
    });