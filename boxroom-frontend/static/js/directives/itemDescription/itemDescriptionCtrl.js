/**
 * Created by AKoryagin on 19.03.2016.
 */
angular.module('boxroom')
    .controller('ItemDescriptionCtrl', ['$scope', 'rentObjectStoreService', '$injector', function ItemDescriptionCtrl($scope, rentObjectStoreService, $injector) {
        console.log($scope);
        $scope.editingItemDescription = false;

        $scope.toggleEditingItem = function() {
            $scope.editingItemDescription = !$scope.editingItemDescription;

        };
        $scope.saveEditingItem = function() {
            if ($scope.item.isNew) {
                $scope.$emit('saveNewItem', $scope.item);
            } else {
                $scope.$emit('putItem', $scope.item);
            }
            $scope.editingItemDescription = false;

        };

        $scope.deleteItem = function() {
            $scope.$emit('deleteItem', $scope.item);
        };

        $scope.$watch('item', function(newVal, oldVal) {
            if (newVal && newVal.isNew) {
                $scope.editingItemDescription = true;
            } else {
                $scope.editingItemDescription = false;
            }
        })
    }]);