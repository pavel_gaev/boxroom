/**
 * Created by AKoryagin on 19.03.2016.
 */
angular.module('boxroom')
    .controller('LeftNavBarCtrl', ['$scope', function LeftNavBarCtrl($scope) {
        $scope.minified = true;

        $scope.toggleMinifyed = function() {
            $scope.minified = !$scope.minified;
        }
    }]);