angular.module('boxroom')
    .directive('leftNavBar', function () {
        return {
            templateUrl: 'static/templates/leftNavBar.html',
            controller: 'LeftNavBarCtrl',
            link: function(scope, element, attrs, ctrl) {

            }
        }
    });