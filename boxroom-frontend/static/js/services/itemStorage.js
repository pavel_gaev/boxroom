angular.module('boxroom')
    .factory('rentObjectStoreService', ['$http','$injector', function($http, $injector) {
        'use strict';
        return $http.get('/api')
            .then(function () {
                return $injector.get('api');
            });
    }])
    .factory('api', ['$http', '$stateParams','VERSION', 'MOK_DATA', function ($http, $stateParams, VERSION, MOK_DATA) {
        'use strict';
        var store = {
            things: [],
            periods: [],

            delete: function (thing) {
                var originalThings = store.things.slice(0);

                store.things.splice(store.things.indexOf(thing), 1);

                return $http.delete('http://pgaev:8080/rentItem/' + thing.id)
                    .then(function success() {
                        return store.things;
                    }, function error() {
                        angular.copy(originalThings, store.things);
                        return originalThings;
                    });
            },

            getItemById: function() {

            },

            getItemPeriods: function(itemId) {
                /*
                * params:
                    * startDate
                    * endDate
                    * itemId
                * */
                var d = new Date(Date.apply(null, arguments));
                d.setDate(1);
                var startDate = moment(d).format('YYYY-MM-DDTHH:mm:ss.SSSZZ');
                d.setMonth(d.getMonth() + 1);
                d.setDate(0);
                var endDate = moment(d).format('YYYY-MM-DDTHH:mm:ss.SSSZZ');
                startDate = encodeURIComponent(startDate);
                endDate = encodeURIComponent(endDate);
                var url =  'http://pgaev:8080/rentPeriod/search/findActivePeriodsByItemIdAndDirectionAndPeriod?id='
                    + itemId
                    + '&direction=OUT'
                    + '&startDate='
                    + startDate
                    + '&endDate='
                    + endDate;
                return $http.get(url)
                    .then(function (resp) {
                        var events = [];

                        angular.copy(resp.data._embedded.periods, store.periods);

                        return store.periods.map(function(item){
                            var calendarEventType;
                            switch (item.status) {
                                case 'DECLINED':
                                    calendarEventType = 'warning';
                                    break;
                                case 'NEW':
                                    calendarEventType = 'info';
                                    break;
                                case "APPROVED":
                                    calendarEventType = 'success';
                                    break;
                                case "RETURNED":
                                    calendarEventType = 'important';
                                    break;
                                case "ISSUED":
                                    calendarEventType = 'special';
                                    break;
                            }
                            var partsOfUrl = item._links.self.href.split('/');
                            var lastPartOfUrl = partsOfUrl[partsOfUrl.length-1];

                            var id = lastPartOfUrl;
                            item = {
                                id: id,
                                title: 'Забронировано', // The title of the event
                                type: calendarEventType, // The type of the event (determines its color). Can be important, warning, info, inverse, success or special
                                startsAt: moment(item.startDate, 'YYYY-MM-DDTHH:mm:ss.SSSZZ').toDate(), // A javascript date object for when the event starts
                                endsAt: item.endDate && moment(item.endDate, 'YYYY-MM-DDTHH:mm:ss.SSSZZ').toDate(), // Optional - a javascript date object for when the event ends
                                editable: true, // If edit-event-html is set and this field is explicitly set to false then dont make it editable.
                                deletable: true, // If delete-event-html is set and this field is explicitly set to false then dont make it deleteable
                                draggable: false, //Allow an event to be dragged and dropped
                                resizable: false, //Allow an event to be resizable
                                incrementsBadgeTotal: true, //If set to false then will not count towards the badge total amount on the month and year view
                                recursOn: 'year' // If set the event will recur on the given period. Valid values are year or month

                            };
                            return item;
                        });

                    })
            },

            searchBySubstr: function(str) {
                return $http.get('http://pgaev:8080/rentItem/search/findByContent?substr=' + str)
                //return $http.get('http://pgaev:8080/rentItem/search/findByOwnerAndContent?substr=' + str + '&id=' + MOK_DATA.USER_ID)
                    .then(function (resp) {
                        angular.copy(resp.data._embedded.items, store.things);
                        return store.things;
                    })
            },
            search: function(str) {
                //return $http.get('http://pgaev:8080/rentItem/search/findByContent?substr=' + str)
                return $http.get('http://pgaev:8080/rentItem/search/findByOwnerAndContent?substr=' + str + '&id=' + MOK_DATA.USER_ID)
                    .then(function (resp) {
                        angular.copy(resp.data._embedded.items, store.things);
                        return store.things;
                    })
            },

            getItems: function () {
                return $http.get('http://pgaev:8080/rentItem/search/findByOwnerId?id=' + MOK_DATA.USER_ID)
                //return $http.get('http://pgaev:8080/rentItem?owner_id=1')
                    .then(function (resp) {
                        console.log(resp.data);
                        angular.copy(resp.data._embedded.items, store.things);
                        store.things.map(function(item){
                            var partsOfUrl = item._links.rentItem.href.split('/');
                            var lastPartOfUrl = partsOfUrl[partsOfUrl.length-1];
                            item.id = lastPartOfUrl;
                            return item;
                        });
                        return store.things;
                    })
            },
            getRequests: function () {
                return $http.get('http://pgaev:8080/rentItem/search/findByOwnerId?id=' + MOK_DATA.USER_ID)
                //return $http.get('http://pgaev:8080/rentItem?owner_id=1')
                    .then(function (resp) {
                        console.log(resp.data);
                        angular.copy(resp.data._embedded.items, store.things);
                        store.things.map(function(item){
                            var partsOfUrl = item._links.rentItem.href.split('/');
                            var lastPartOfUrl = partsOfUrl[partsOfUrl.length-1];
                            item.id = lastPartOfUrl;
                            return item;
                        });
                        return store.things;
                    })
            },

            insert: function (thing) {
                var originalThings = store.things.slice(0, store.things.length - 1);
                var objectToSend;
                objectToSend = angular.copy(thing);
                delete objectToSend.isNew;
                return $http({
                    method: 'POST',
                    url: 'http://pgaev:8080/rentItem',
                    data: objectToSend
                }).then(function success(resp) {
                    var partsOfUrl = resp.data._links.rentItem.href.split('/');
                    var lastPartOfUrl = partsOfUrl[partsOfUrl.length-1];
                    thing.id = lastPartOfUrl;
                    thing.isNew = false;
                    return store.things;
                }, function error() {
                    angular.copy(originalThings, store.things);
                    return store.things;
                });

            },

            put: function (thing) {
                var originalThings = store.things.slice(0);
                var objectToSend;
                objectToSend = angular.copy(thing);
                return $http.put('http://pgaev:8080/rentItem/' + thing.id, thing)
                    .then(function success() {
                        return store.things;
                    }, function error() {
                        angular.copy(originalThings, store.things);
                        return store.things;
                    });
            }
        };

        return store;
    }]);