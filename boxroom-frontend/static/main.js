/**
 * Created by AKoryagin on 19.03.2016.
 */
angular.module('boxroom', ['ui.router', 'mwl.calendar', 'ui.bootstrap'])
    .constant(
        "VERSION", {
            "CURRENT_VERSION": "1.0"
        }
    )

    .constant ("MOK_DATA", {
        "USER_ID": "-1"
    })
    .config(function(calendarConfig) {

        console.log(calendarConfig); //view all available config

        calendarConfig.dateFormatter = 'moment'; //use either moment or angular to format dates on the calendar. Default angular. Setting this will override any date formats you have already set.

        calendarConfig.allDateFormats.moment.date.hour = 'HH:mm'; //this will configure times on the day view to display in 24 hour format rather than the default of 12 hour

        calendarConfig.allDateFormats.moment.title.day = 'ddd D MMM'; //this will configure the day view title to be shorter

        calendarConfig.i18nStrings.weekNumber = 'Week {week}'; //This will set the week number hover label on the month view

        calendarConfig.displayAllMonthEvents = true; //This will display all events on a month view even if they're not in the current month. Default false.

        calendarConfig.displayEventEndTimes = false; //This will display event end times on the month and year views. Default false.

        calendarConfig.showTimesOnWeekView = true; //Make the week view more like the day view, with the caveat that event end times are ignored.

    })
    .config(function ($stateProvider, $urlRouterProvider) {
        'use strict';
        $urlRouterProvider.otherwise("/");
        var resolveItems = {
            rentObjectsStore: function (rentObjectStoreService) {
                // Get the correct module (API or localStorage).

                return rentObjectStoreService.then(function (module) {
                    module.getItems(); // Fetch the todo records in the background.
                    return module;
                });

            }
        };
        var resolveRequests = {
            rentObjectsStore: function (rentObjectStoreService) {
                // Get the correct module (API or localStorage).

                return rentObjectStoreService.then(function (module) {
                    module.getRequests(); // Fetch the todo records in the background.
                    return module;
                });

            }
        };
        var routeConfigItems = {

            controller: 'MainLoggedUserPageCtrl',
            //templateUrl: 'mainLoggedUserPage',
            templateUrl: 'static/templates/mainLoggedUserPage.html',
            resolve: resolveItems
        };
        var routeConfigRequests = {

            controller: 'MainCustomerUserPageCtrl',
            //templateUrl: 'mainLoggedUserPage',
            templateUrl: 'static/templates/MainCustomerUserPage.html',
            resolve: resolveRequests
        };
        var resolveViewCalendar = {
            rentObjectsStore: ['rentObjectStoreService', '$stateParams', function (rentObjectStoreService, $stateParams) {
                // Get the correct module (API or localStorage).

                return rentObjectStoreService.then(function (module) {
                    //module.getItemPeriods($stateParams.itemId);
                    //module.getItemPeriods($stateParams.itemId);
                    return module.getItemPeriods($stateParams.itemId);
                    //return module;

                });

            }]
        };
        $stateProvider
            .state('/items', angular.extend({url: '/items'}, routeConfigItems))
            .state('/requests', angular.extend({url: '/requests'}, routeConfigRequests))
            .state('/viewCalendar', {
                url: '/viewCalendar/:itemId',
                controller: 'CalendarCtrl',
                templateUrl: 'static/templates/viewCalendar.html',
                resolve: resolveViewCalendar
            });
    });