package org.cafejava.boxroom.controller;

import org.cafejava.boxroom.controller.dto.FileItemDto;
import org.cafejava.boxroom.service.FileItemService;
import org.cafejava.boxroom.utils.FileHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.controller
 */
@Controller
@RequestMapping("fileUpload")
public class FileItemController {

    private static final Logger logger = LoggerFactory.getLogger(FileItemController.class);

    @Autowired
    private FileItemService fileItemService;

    @RequestMapping(value = "/attachments/{fileId}", method = RequestMethod.GET)
    public void getFile(@PathVariable Long fileId, HttpServletResponse response) {
        boolean result = false;
        FileItemDto file = fileItemService.getById(fileId);
        if (file != null) {
            File dataFile = new File(file.getPath());
            result = putFileToResponse(response, dataFile, file.getOriginalName());
        }

        if (!result) {
            logger.error("Cannot return file with id = " + fileId);
            putFileToResponse(response, null, null);
        }
    }

    private boolean putFileToResponse(HttpServletResponse response, File file, String fileName) {
        boolean result = true;
        if (file != null) {
            String type = URLConnection.getFileNameMap().getContentTypeFor(fileName);
            response.setContentType(type == null ? "application/octet-stream" : type);
            response.setContentLength((int) file.length());
            InputStream is = null;
            try {
                is = new FileInputStream(file.getPath());
                FileCopyUtils.copy(is, response.getOutputStream());
            } catch (IOException e) {
                result = false;
            } finally {
                FileHelper.closeStreamSilently(is);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
        }
        return result;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseBody
    public FileItemDto uploadFileItem(@RequestParam(value = "file", required = true) MultipartFile file) {
        try {
            FileItemDto fileItemDto = fileItemService.save(file);
            return fileItemDto;
        } catch (RuntimeException e) {
            logger.error("Error while uploading.", e);
            throw e;
        } catch (Exception e) {
            logger.error("Error while uploading.", e);
            throw new RuntimeException(e);
        }
    }
}
