package org.cafejava.boxroom.controller.dto;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.controller.dto
 */
public class FileItemDto {
    Long id;
    String originalName;
    String path;
    Long size;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }
}
