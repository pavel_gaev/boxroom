package org.cafejava.boxroom.dao;

import org.cafejava.boxroom.domain.FileItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.dao
 */
@RepositoryRestResource(collectionResourceRel = "items", path = "fileItem")
public interface FileItemRepository extends PagingAndSortingRepository<FileItem, Long> {
}
