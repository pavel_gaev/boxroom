package org.cafejava.boxroom.dao;

import org.cafejava.boxroom.domain.RentDirection;
import org.cafejava.boxroom.domain.RentItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 19.03.2016
 * Time: 15:04
 */
@RepositoryRestResource(collectionResourceRel = "items", path = "rentItem")
public interface RentItemRepository extends PagingAndSortingRepository<RentItem, Long> {

    List<RentItem> findByOwnerId(@Param("id") Long ownerId);

    @Query(value = "select ri from RentItem ri " +
            "where ri.name like concat('%', :substr,'%') " +
            "or ri.description like concat('%', :substr,'%')")
    List<RentItem> findByContent(@Param("substr") String substr);

    @Query(value = "select ri from RentItem ri " +
            "where ri.ownerId = :id " +
            "and ( ri.name like concat('%', :substr,'%') " +
            "or ri.description like concat('%', :substr,'%')) " +
            "or ri.barcode = :substr")
    List<RentItem> findByOwnerAndContent(@Param("id") Long ownerId,@Param("substr") String substr);

    @Query(value = "select ri from RentItem ri " +
            "left join ri.periods as period ON period.direction = :direction and period.startDate < :endDate " +
            "and (period.endDate is null or period.endDate> :startDate) " +
            "where ri.name like concat('%', :substr,'%') " +
            "or ri.description like concat('%', :substr,'%')" +
            "group by ri " +
            "having count(period)=0")
    List<RentItem> findByContent2(@Param("substr") String substr, @Param("direction") RentDirection direction, @Param("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date startDate, @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") @Param("endDate") Date endDate);
}
