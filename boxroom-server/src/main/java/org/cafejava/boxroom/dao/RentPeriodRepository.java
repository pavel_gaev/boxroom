package org.cafejava.boxroom.dao;

import org.cafejava.boxroom.domain.RentDirection;
import org.cafejava.boxroom.domain.RentPeriod;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 19.03.2016
 * Time: 17:01
 */
@RepositoryRestResource(collectionResourceRel = "periods", path = "rentPeriod")
public interface RentPeriodRepository extends PagingAndSortingRepository<RentPeriod, Long> {
    List<RentPeriod> findByItemId(@Param("id") Long itemId);

    @Query(value = "Select rp from RentPeriod rp where rp.itemId = :id and rp.isActive = true")
    List<RentPeriod> findActivePeriodsByItemId(@Param("id") Long itemId);

    @Query(value = "Select rp from RentPeriod rp where rp.itemId = :id  and rp.direction = :direction and rp.isActive = true")
    List<RentPeriod> findActivePeriodsByItemIdAndDirection(@Param("id") Long itemId, @Param("direction") RentDirection direction);

//    @Query(value = "Select rp from RentPeriod rp where rp.itemId = :id  and rp.direction = :direction and rp.isActive = true")
//    List<RentPeriod> findActivePeriodsByItemIdAndDirection(@Param("id") Long itemId, @Param("direction") RentDirection direction);

    @Query(value = "Select rp from RentPeriod rp " +
            "where rp.itemId = :id  " +
            "and rp.direction = :direction " +
            "and rp.isActive = true " +
            "and rp.startDate < :endDate " +
            "and (rp.endDate > :startDate or rp.endDate is null)")
    List<RentPeriod> findActivePeriodsByItemIdAndDirectionAndPeriod(@Param("id") Long itemId, @Param("direction") RentDirection direction, @Param("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") Date startDate, @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ") @Param("endDate") Date endDate);

}
