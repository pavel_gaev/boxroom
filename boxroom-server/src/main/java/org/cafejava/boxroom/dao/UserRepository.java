package org.cafejava.boxroom.dao;


import org.cafejava.boxroom.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by Pavel.
 * Date: 19.03.2016 0:00.
 */
@RepositoryRestResource(collectionResourceRel = "users", path = "user")
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    List<User> findByName(@Param("name") String name);

    @Query(value = "select  u from User u where u.name like concat('%', :substr,'%') " +
            "or u.email like concat('%', :substr,'%')")
    List<User> findBySubstring(@Param("substr") String substr);

}
