package org.cafejava.boxroom.domain;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 19.03.2016
 * Time: 15:47
 */
public enum RentDirection {
    IN, OUT
}
