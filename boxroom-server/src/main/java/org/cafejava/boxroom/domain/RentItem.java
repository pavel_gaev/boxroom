package org.cafejava.boxroom.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 19.03.2016
 * Time: 14:06
 */
@Entity
@Table
public class RentItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private Long ownerId;
    @Column
    private String name;
    @Column
    private String description;

    @Column
    private String barcode;

    @Column(name = "active")
    private boolean isActive = false;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentItem")
    Collection<RentPeriod> periods;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Collection<RentPeriod> getPeriods() {
        return periods;
    }

    public void setPeriods(Collection<RentPeriod> periods) {
        this.periods = periods;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
}
