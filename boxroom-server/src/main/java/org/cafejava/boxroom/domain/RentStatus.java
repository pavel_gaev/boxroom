package org.cafejava.boxroom.domain;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 19.03.2016
 * Time: 18:44
 */
public enum RentStatus {
    NEW,
    APPROVED,
    DECLINED,
    ISSUED,
    RETURNED
}
