package org.cafejava.boxroom.service;

import org.cafejava.boxroom.controller.dto.FileItemDto;
import org.cafejava.boxroom.domain.FileItem;
import org.springframework.web.multipart.MultipartFile;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.service
 */
public interface FileItemService {

    FileItemDto save(MultipartFile file);

    FileItemDto getById(Long id);


}
