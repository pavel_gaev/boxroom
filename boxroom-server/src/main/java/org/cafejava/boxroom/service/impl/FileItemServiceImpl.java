package org.cafejava.boxroom.service.impl;

import org.cafejava.boxroom.controller.FileItemController;
import org.cafejava.boxroom.controller.dto.FileItemDto;
import org.cafejava.boxroom.dao.FileItemRepository;
import org.cafejava.boxroom.domain.FileItem;
import org.cafejava.boxroom.service.FileItemService;
import org.cafejava.boxroom.utils.FileHelper;
import org.cafejava.boxroom.utils.FileItemConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.service.impl
 */
@Service
public class FileItemServiceImpl implements FileItemService {

    private static final Logger logger = LoggerFactory.getLogger(FileItemServiceImpl.class);

    @Autowired
    private FileItemRepository fileItemRepository;

    @Value("${file_storage.files}")
    private String fileStoragePath;

    @Override
    @Transactional
    public FileItemDto save(MultipartFile file) {
        String mfName = null;
        FileItem item = null;
        try {
            mfName = file.getName();
            item = saveFile(file);
            FileItem fileItem = fileItemRepository.save(item);
            return FileItemConverter.toDto(fileItem, fileStoragePath);
        } catch (IOException ex) {
            logger.error("Cannot save the file \"" + mfName + "\"");
        }
        return null;
    }

    @Override
    @Transactional
    public FileItemDto getById(Long id) {
        FileItem fileItem = fileItemRepository.findOne(id);
        return fileItem == null ? null : FileItemConverter.toDto(fileItem, fileStoragePath);
    }

    private FileItem saveFile(MultipartFile multipartFile) throws IOException {
        String fileName = multipartFile.getOriginalFilename();
        String storageFileName = getUniqueFilename(fileName);

        logger.debug("Copy to storage starts");
        copyToStorage(multipartFile, storageFileName);
        logger.debug("Copy to storage ends");

        FileItem item = new FileItem();
        item.setOriginalFileName(fileName);
        item.setStorageFileName(storageFileName);
        item.setSize(multipartFile.getSize());
        item.setDocumentDate(new Date());
        item.setItemId(-1l);    //fake item id. it is need update after save item
        return item;
    }

    protected String getStorageFilePath(String storagePath, String storageFileName) {
        if (!storageFileName.startsWith(File.separator))
            storageFileName = File.separator + storageFileName;
        return storagePath + storageFileName;
    }

    protected void copyToStorage(MultipartFile multipartFile, String storageFileName) throws IOException {
        File dstFile = new File(getStorageFilePath(fileStoragePath, storageFileName));
        FileHelper.createEmptyFile(dstFile);
        multipartFile.transferTo(dstFile);
    }

    public static String getUniqueFilename(String originFilename) {
        String fileName = UUID.randomUUID().toString();
        int dotIdx = originFilename.lastIndexOf(".");
        if(dotIdx != -1)
            fileName += originFilename.substring(dotIdx);
        return fileName;
    }
}
