package org.cafejava.boxroom.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.utils
 */
public class FileHelper {

    public static boolean createEmptyFile(File file) {
        if (!file.exists()) {
            File dir = file.getParentFile();
            if (!dir.exists()) {
                dir.mkdirs();
            }

            if (dir.exists()) {
                try {
                    file.createNewFile();
                } catch (IOException ignore) { }
            }
        }

        return file.exists();
    }

    public static void closeStreamSilently(InputStream is) {
        if (is != null) {
            try {
                is.close();
            } catch (Exception e) {/*nothing to do here*/}
        }
    }
}
