package org.cafejava.boxroom.utils;

import org.cafejava.boxroom.controller.dto.FileItemDto;
import org.cafejava.boxroom.domain.FileItem;

import java.io.File;

/**
 * Short description text.
 * <p/>
 * Long detailed description text for the specific class file.
 *
 * @author SSukhanov
 * @version 19.03.2016
 * @package org.cafejava.boxroom.utils
 */
public class FileItemConverter {

    public static FileItemDto toDto(FileItem item, String fileStoragePath) {
        FileItemDto dto = new FileItemDto();
        dto.setId(item.getId());
        dto.setOriginalName(item.getOriginalFileName());
        dto.setSize(item.getSize());
        dto.setPath(resolveStoragePath(item.getStorageFileName(), fileStoragePath));
        return dto;
    }

    private static String resolveStoragePath(String storageFileName, String fileStoragePath) {
        if(!(storageFileName).startsWith(File.pathSeparator)) {
            storageFileName = File.separator + storageFileName;
        }
        return fileStoragePath + storageFileName;
    }
}
